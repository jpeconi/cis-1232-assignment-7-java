
import java.util.Scanner;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author bjmaclean
 */
public class Room extends RoomBase implements Bookable{

    Scanner input = new Scanner(System.in);
    private String reservedBy = "";
    private boolean reserved;
    private boolean hasSmartBoard;

    /**
     * Get the attribute values from the user.
     *
     * @param roomNumber
     */
    public Room(int roomNumber) {
        super(roomNumber);
    }

    @Override
    public void getRoomDetailsFromUser() {

        System.out.print("Enter number of seats: ");
        super.setNumberOfSeats(input.nextInt());
        input.nextLine();
        System.out.print("Does this classroom have a smart board? (Y/N)");
        hasSmartBoard = input.nextLine().equalsIgnoreCase("y");

    }

    public boolean isHasSmartBoard() {
        return hasSmartBoard;
    }

    public void setHasSmartBoard(boolean hasSmartBoard) {
        this.hasSmartBoard = hasSmartBoard;
    }

    public String getReservedBy() {
        return reservedBy;
    }

    public void setReservedBy(String reservedBy) {
        this.reservedBy = reservedBy;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }

    /**
     * Update the room to reserved and get the reserved by.
     */
    @Override
    public void reserveThisRoom() {
        this.reserved = true;
        System.out.println("Enter the name of the person reserving this room: ");
        reservedBy = input.nextLine();
    }

    /**
     * Update the room to not reserved and clear the reserved by.
     */
    @Override
    public void releaseThisRoom() {
        this.reserved = false;
        reservedBy = "";
        System.out.println("Room has been released\n");

    }

    @Override
    public String toString() {
        String output = "\n******************************"
                + "\nRoom Number: " + super.getRoomNumber()
                + "\nNumber of Seats: " + super.getNumberOfSeats()
                + "\nReserved By: " + reservedBy
                + "\nReserved: " + reserved
                + "\nSmart Board: " + hasSmartBoard;
        return output;
    }

}
