
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Ryan
 * @since March 23, 2014
 *
 * This class is used for counting the rooms as well as searching for a room
 * that suits the users need.
 *
 * ############# Update to this method ##################
 * @author Jamison Peconi
 * @since 03/27/2016
 * Assignment 7
 *
 * Added the ability to handle ties with the room size. Converted the int
 * largestRoom variable to a String for the ability to concatenate a number of
 * different rooms.
 */
public class RoomUtility {

    public static void roomCount(ArrayList<Room> rooms) {
        int numOfRoom = 0;
        int numOfComputerRoom = 0;
        int numOfBoardRoom = 0;
        int numOfBoardRoomPro = 0;
        int numOfBiologyRoom = 0;
        // Change this variable to a string to be able to concatenate the room numbers in the event of a tie
        String largestRoom = "";
        int largestNumOfRooms = 0;
        for (int i = 0; i < rooms.size(); i++) {

            System.out.println("This room is a: " + rooms.get(i).getClass().toString());
            if (rooms.get(i).getClass().toString().equals("class Room")) {
                numOfRoom += 1;
            }
            if (rooms.get(i).getClass().toString().equals("class ComputerRoom")) {
                numOfComputerRoom += 1;
            }
            if (rooms.get(i).getClass().toString().equals("class BoardRoom")) {
                numOfBoardRoom += 1;
            }
            if (rooms.get(i).getClass().toString().equals("class BiologyLab")) {
                numOfBiologyRoom += 1;
            }
            if (rooms.get(i).getClass().toString().equals("class BoardRoomPro")) {
                numOfBoardRoomPro += 1;
            }
            
            // This conditional statement was changed to accept a tie. 
            // Also converted the largest Room from an int to a String.
            if (rooms.get(i).getNumberOfSeats() > largestNumOfRooms) {//Determine largest room
                largestNumOfRooms = rooms.get(i).getNumberOfSeats();
                largestRoom = Integer.toString(rooms.get(i).getRoomNumber()); // Changed this variable over to convert the int to a string
            } else if (rooms.get(i).getNumberOfSeats() == largestNumOfRooms) { // Added statement to handle ties.                 
                largestRoom += ", " + Integer.toString(rooms.get(i).getRoomNumber());
            }
        }
        //Display the results of counting 

        //############# Updated by Jamison Peconi 03/27/16 ###################################
        // Added this local variable to format the output to plurals in the event of a tie of room size.
        // Uses a conditional to determine which sentence to stick into the summary output
        String largestRoomOutput;
        if (largestRoom.contains(",")) {
            largestRoomOutput = "\n\nThe largest rooms are #'s " + largestRoom + " with " + largestNumOfRooms + " seats";
        } else {
            largestRoomOutput = "\n\nLargest Room is #" + largestRoom + " with " + largestNumOfRooms + "seats";
        }

        System.out.println("Room Count Details Report"
                + "\nRooms: " + numOfRoom
                + "\nComputer Rooms: " + numOfComputerRoom
                + "\nBiology Labs: " + numOfBiologyRoom
                + "\nBoard Rooms: " + numOfBoardRoom
                + "\nFancy Board Rooms: " + numOfBoardRoomPro
                + largestRoomOutput
                + "\n");
    }

    public static void roomSearch(ArrayList<Room> rooms) {
        Scanner input = new Scanner(System.in);
        int roomTypeNum = 0;
        do {
            System.out.println("1) Room\n"
                    + "2) Computer Lab\n"
                    + "3) Board Room\n"
                    + "4) Biology lab\n"
                    + "5) Fancy Board Room");
            roomTypeNum = input.nextInt();
        } while (roomTypeNum < 1 || roomTypeNum > 5);
        String roomTypeString = "";

        if (roomTypeNum == 1) {
            roomTypeString = "class Room";
        }
        if (roomTypeNum == 2) {
            roomTypeString = "class ComputerRoom";
        }
        if (roomTypeNum == 3) {
            roomTypeString = "class BoardRoom";
        }
        if (roomTypeNum == 4) {
            roomTypeString = "class BiologyLab";
        }
        if (roomTypeNum == 5) {
            roomTypeString = "class BoardRoomPro";
        }

        System.out.println("How many seats do you need?");
        int numOfSeats = input.nextInt();
        for (int i = 0; i < rooms.size(); i++) {
            if (rooms.get(i).getClass().toString().equals(roomTypeString)) {//If it is the right type of room..
                if (!rooms.get(i).isReserved()) {                           //<--and the room is not reserved.. 
                    if (rooms.get(i).getNumberOfSeats() >= numOfSeats) {    //<-- and it also has the right number of seats or more.
                        System.out.println("\n\n******************************"
                                + "\nRoom Number: " + rooms.get(i).getRoomNumber()
                                + "\nNumber of Seats: " + rooms.get(i).getNumberOfSeats()
                                + "\nSmart Board: " + rooms.get(i).isHasSmartBoard());
                    }
                }
            }

        }

    }

}
