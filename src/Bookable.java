/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jay
 */
public interface Bookable {
    
    public abstract void reserveThisRoom();
    public abstract void releaseThisRoom();
}
