
import java.util.Comparator;

/**
 *
 * @author Jamison Peconi
 * @since 04/06/2016
 *
 * This is a comparator which will compare the Rooms by the room number. This
 * class implements the comparator.
 */
public class RoomNumberComparator implements Comparator {

    /**
     * @author Jamison Peconi
     * @since 04/04/2016
     * @param o1
     * @param o2
     * @return
     *
     * This is the compare method which overrides the default compare method
     * which belongs to comparator. This method will cast the default objects
     * into rooms. It will then subtract the 1st room's number by the second
     * room's number and then return the result. It will sort the array list
     * based on whether it returns a positive number or a negative.
     */
    @Override
    public int compare(Object o1, Object o2) {
        Room r1 = (Room) o1;
        Room r2 = (Room) o2;

        return r1.getRoomNumber() - r2.getRoomNumber();
    }

}
