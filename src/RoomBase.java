
/**
 *
 * @author Jay
 */
public abstract class RoomBase {
    
    private int roomNumber;
    private int numberOfSeats;
    
    public RoomBase(int roomNumber) {
        this.roomNumber = roomNumber;
    }
    
    public abstract void getRoomDetailsFromUser();

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }
 
    
    
    
}
